#define  GPIO2_PREFER_SPEED    1
#include <arduino2.h>
#include <TimerOne.h>
#include <Ethernet.h>
//int pinEncoder[4][2]={{0,0}{1,1}{2,2}{3,3}}; 
//int pinEncoder1[4]={14,24,34,44}; //распиновка условная. Нужно менять при сборке. Каждый двигатель должен соответствовать своему энкодеру.
//int pinEncoder2[4]={15,25,35,45};
//pinEngine[4][3]={{0,0,0}{1,1,1}{2,2,2}{3,3,3}};
int engPinStep[1]={20};
int engPinDir[1]={21};
int engPinEn[1]={22};
int pinOpen[1] = {2};
EthernetClient client;


byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 1, 179);
//IPAddress myDns(192,168,1, 1);
//IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

EthernetServer server(400);
boolean alreadyConnected = false; // whether or not the client was connected previously


int inSize= 0; // Переменная которая будет содержать размер буфера
char str1[256];
char* str; // Так как типа string тут нет, будем использовать массив символов
int valuePinOpen = 0;
int valuePinClose = 0;
boolean er=false;
byte axis; // ось. 0-ось не выбрана.
long int par[5];
long int destination[1]={0}; //конечная точка
boolean forwardDiretion[1]={1};
boolean tagFind[1]={0};



class Engine
{	
	//long destination; //конечная точка
	//int engPin[3]; //пины
	int stepPin;
	int dirPin;
	int enPin;
	int engState;
	//int mDuration; //продолжительность движения
	//int sDuration; //продолжительность остановки
	byte number; //номер двигателя
	boolean inv;
	unsigned long previousMillis;
	unsigned long currentMillis; // текущее время в миллисекундах

	public: 
    byte stat; //битовое поле состояния
    boolean power; //питание (state)
    byte erCode; //код ошибки
	 volatile long int pos;
	
  
	Engine(int pin1, int pin2, int pin3, byte num, boolean invert=0, byte st=0, byte erC=0) //распиновка, номер двигателя,направление, состояние, код ошибки
	{
		stepPin = pin1;
		dirPin = pin2;
		enPin = pin3;
		number = num;
		pinMode2(stepPin, OUTPUT); 
		pinMode2(dirPin, OUTPUT);
		pinMode2(enPin, OUTPUT);
		previousMillis = millis();
		SetPower(0);
		stat = st;
		erCode = erC;
		power=0;
		pos=0;
		inv=invert;
	}


	void SetPower(boolean p) //установка питания
	{
		power = p; 
		if(p) digitalWrite2(enPin, HIGH);
		else digitalWrite2(enPin, LOW);
	}
	void SetErCode(byte erC) //установка кода ошибки
	{
		stat = 1;
		erCode = erC;
	}
	void SetStat(byte st) //установка состояния двигателя
	{
		stat = st;
	}
	void Move(boolean forDir, float m=0.1, float s=0.1) //направление (true = вперед, false = назад), задержки заданы по-умолчанию, но возможно переназначение по ситуации. 
	{ 
		forDir=forDir ^ inv;
		if(forDir) digitalWrite2(dirPin, LOW);
		else digitalWrite2(dirPin, HIGH);
		currentMillis = millis(); // текущее время в миллисекундах 
		if(engState == HIGH) 
		{   
			engState = LOW; // выключаем 
			digitalWrite2(stepPin, LOW);// реализуем новое состояние
		} 
		else if (engState == LOW) 
		{ 	  
			if(((forDir)&&(!inv)) || ((!forDir)&&(inv))) pos--;
			else pos++;
			engState = HIGH; // выключаем 
			digitalWrite2(stepPin, HIGH); 
		} 
	}
};

Engine eng[1] = 
{
	Engine(engPinStep[0],engPinDir[0],engPinEn[0],1),//распиновка, номер двигателя. 1 после номера, если направление обратное.
};

void setup() 
{
	// initialize the ethernet device
	Ethernet.begin(mac, ip, subnet);
	// start listening for clients
	server.begin();  
	Timer1.initialize(); 
	Timer1.setPeriod(100);
	Timer1.attachInterrupt(timerIsr); 
	//Timer3.initialize();
	///Timer3.setPeriod(1);
	//Timer3.attachInterrupt( timerIsr2 ); 
	//Serial.begin(9600);// Открываем порт с скоростью передачи в 9600 бод(бит/с)
	pinMode2(pinOpen[0], INPUT);
 
}

void Idn() //далее идут команды. Смотреть ТЗ.
{
	client.print("Model: ");
	client.print("Arduino MEGA, smal");
	client.print("ver: ");
	client.println("4.00");
}

void StSet(byte ax=0, byte s=0, byte c=0) //дефолтно работает как Clear. 
{
		eng[0].SetStat(s);
		eng[0].SetErCode(c);
}

void EnRead(byte p=0) 
{
	client.println(eng[0].power);
}

void En(byte p=0, boolean ch=0) 
{
	eng[0].SetPower(ch);;
}

void Stat(byte p=0) //+
{
	byte stat=eng[0].stat; 
	client.print(stat);
}


void Err(byte p=0) //+
{
	byte code=eng[0].erCode; client.println(code);
}


void Clr(byte p=0) 
{
	eng[0].SetErCode(0);
	eng[0].SetStat(0);
}

void SetError(byte ch, byte p=0)
{
	eng[0].SetErCode(ch);
}

void FindTag(byte p) //поиск опорной метки
{
	forwardDiretion[0]=1;
	eng[0].SetStat(4);
	tagFind[0]=1;
	destination[0]=-99999999999999;
}
void MoveTo(byte p) //движение с выбранной осью. Тут выставляется направление, а само движение идет по таймеру.
{
	long int posit=eng[0].pos;
	if(posit>destination[0])
	{
		forwardDiretion[0]=1;
		eng[0].SetStat(4);
		
	}
	else if(posit<destination[0])
		{
			forwardDiretion[0]=0;
			eng[0].SetStat(4);
			//client.print("dvoechka");
		}
		else {eng[0].SetStat(3);} 
}

void Abort(byte p=0)
{
	eng[0].SetStat(0);
}

void Pos(byte p=0)
{
	long int g=eng[0].pos;
	client.println(g);
}


boolean Command() //тут происходит вызов всех команд. Лучше даже не заглядывать. Пока что(на время отладки) вызовы команд сопровождаются странными сообщениями для однозначного определения происходящего во время выполнения.
{	
	boolean error=true;
	byte param=eng[par[0]].stat;
	if (param!=1)
	{
		if(strcmp(str,"IDN?")==0)
		{
			error=false;		
			Idn();
		}
		if((strcmp(str,"EN?")==0)||(strcmp(str,"EN*?")==0))
		{
			error=false;		
			EnRead(par[0]);			
		}
		if((strcmp(str,"EN* *")==0)||(strcmp(str,"EN *")==0))
		{
			error=false;
			if(par[1]<3) //STATE 1 или 2.
			{
				En(par[0],par[1]);
			}
			else {error=false; eng[par[0]].SetErCode(4);} //ошибка 4
		}
		if((strcmp(str,"STAT?")==0)||(strcmp(str,"STAT*?")==0))
		{
			error=false;
			//client.println("STAT");
			Stat(par[0]);
		}
		if((strcmp(str,"ERR?")==0)||(strcmp(str,"ERR*?")==0)) //!!!!!!!!!! выводит без вызова
		{
			error=false;
			//client.println("ERR21");
			Err(par[0]);
		}
		if((strcmp(str,"CLR")==0)||(strcmp(str,"CLR*")==0))
		{
			error=false;
			//client.println("CLR");
			Clr(par[0]);			
		}
		if((strcmp(str,"MH")==0)||(strcmp(str,"MH*")==0))// двигаться, пока не достигнем опорной метки
		{
			error=false;
			//client.println("MH");
			FindTag(par[0]);
		}
		if((strcmp(str,"ABORT")==0)||(strcmp(str,"ABORT*")==0))
		{
			error=false;
			//client.println("ABORT");
			Abort(par[0]);
		}
		if((strcmp(str,"POS?")==0)||(strcmp(str,"POS*?")==0))
		{
			error=false;
			//client.println("POS");
			Pos(par[0]);
		}
		if((strcmp(str,"MOVE* *")==0)||(strcmp(str,"MOVE *")==0))// для одной оси
		{
			error=false;
			destination[par[0]-1]=par[1];			
			MoveTo(par[0]);
		}
	}
	else
	{
		if((strcmp(str,"CLR")==0)||(strcmp(str,"CLR*")==0))
		{
			error=false;
	
			Clr(par[0]);			
		}
		if((strcmp(str,"ABORT")==0)||(strcmp(str,"ABORT*")==0))
		{
			error=false;
	
			Abort(par[0]);
		}
	}
	return error;
}

void AnalCom() //кажется завершенной. Ошибки применяются ко всем осям, т.к. ось еще не является выбранной. 
{
	int i=0;
	int k=0;
	int nCount=0;
	int nPos=0;
	axis=0;
	byte d=0;
	long int ex=1;	 
	par[0]=0;//смотреть MOVE в ТЗ (нужно для записи координат в разные оси))
	while(i<inSize) //идем по строке
	{
		if (str[i] == ' ') {d=1; if (d>=2) {StSet(0,1,5);break;}}//Если число параметров превышает заданное.
		if((str[i] >= '0') && (str[i] <= '9')) //если встретили число
		{
			nCount=0;
			nPos=i;
			while((str[i] >= '0') && (str[i] <= '9'))
			{        
				i++;
				nCount++;
				if(i>=inSize) break;
			}
			for (int j=i-1; j>=nPos; j--)
			{
				par[d]+=(str[j]-'0')*ex;
				ex*=10;
			}
			str[k++]='*';
			//client.println(str);
			d++;
			if (d>=2) {StSet(0,1,5);break;}//Если число параметров превышает заданное.
			ex=1;
		}
		str[k++]=str[i++];
		//client.println(str[i]);
	}
	for(int j=k; j<inSize+1; j++) str[j]='\0';
	//client.println(str);
	if (par[0]>2) StSet(0,1,5); //ось не найдена(их не больше 2) 
} 

//ISR (TIMER1_COMPB_vect)
void timerIsr()
{
    // Обработчик прерывания таймера 0
	if(!forwardDiretion[0]) //смотрим направление движения
	{    //вперед
		if(eng[0].pos>=destination[0])  eng[0].SetStat(3);  //если дальше нужного положения, то останавливаем		
	}
	else 
	{    //назад
		if(eng[0].pos<=destination[0])  eng[0].SetStat(3);	//если ближе нужного положения, то останавливаем 
	}
	if((eng[0].stat)==4)  // если в движении, то едем дальше.
	{
		eng[0].Move(forwardDiretion[0]); //едем в нужном направлении
	}
	if(tagFind[0]) //если идет поиск опорной метки
	{
		if(!(digitalRead2(pinOpen[0]))) //при нахождении остановится и обнулить позицию
		{
			eng[0].SetStat(2);
			eng[0].pos=0;
			tagFind[0]=0;
		}
	}
}

/*void timerIsr2()
//ISR (TIMER2_COMPA_vect)
{
	// Обработчик прерывания таймера 1
	for(volatile byte engNum=0; engNum<=3; engNum++) 
	{
		enc[engNum].Update();
	}
}*/

void loop()
{
	Serial.flush();
	inSize=0; // Сбрасываем переменную
	memset(str1, '\0', sizeof(str1)); // Очищаем массив
	client = server.available();
	if (client)
	{	  
		alreadyConnected=true;		
		if(client.available()>0)
		{        
			inSize =client.readBytesUntil('\n',str1,sizeof(str1));
			str=str1;

			if (inSize>0 && str[inSize-1]=='\r')
			{
				str[inSize-1]=0;
				inSize--;		  
			}
			if(str[0]=='\r') str++;  
		}
		AnalCom();
		if (Command()) 
		{
			SetError(1); 
			client.println("Command not found!");
			client.println("Command not found!");
		}    
	}
}
